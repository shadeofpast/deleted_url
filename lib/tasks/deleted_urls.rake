namespace :deleted_urls do
  desc "Shorten long url chains"
  task shorten: :environment do
    ChainShortener.new.call
  end

  desc "Remove cyclic redirect"
  task remove_cycles: :environment do
    CyclicRedirectRemover.new.call
  end
end
