FactoryBot.define do
  factory :deleted_url do
    sequence(:prev_url) { |i| "prev_#{i}" }
    sequence(:next_url) { |i| "next_#{i}" }

    factory :chained_deleted_url do
      transient  do
        chain_size { 12 }
      end

      after(:create) do |deleted_url, evaluator|
        next_url = deleted_url.next_url
        (evaluator.chain_size - 1).times do
          next_url = create(:deleted_url, prev_url: next_url).next_url
        end
      end
    end

    factory :cyclic_deleted_url do
      transient  do
        chain_size { 3 }
      end

      after(:create) do |deleted_url, evaluator|
        prev_url = deleted_url.prev_url
        next_url = deleted_url.next_url
        (evaluator.chain_size - 2).times do
          next_url = create(:deleted_url, prev_url: next_url).next_url
        end
        create(:deleted_url, prev_url: next_url, next_url: prev_url)
      end
    end
  end
end
