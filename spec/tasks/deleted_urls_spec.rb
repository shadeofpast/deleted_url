require 'rails_helper'

Rails.application.load_tasks

RSpec.describe 'deleted_urls.rake' do
  describe ':shorten' do
    it 'call proper service' do
      service = instance_double(ChainShortener, call: nil)
      expect(service).to receive(:call)
      allow(ChainShortener).to receive(:new).and_return(service)
      Rake::Task["deleted_urls:shorten"].invoke
    end
  end

  describe ':remove_cycles' do
    it 'call proper service' do
      service = instance_double(CyclicRedirectRemover, call: nil)
      expect(service).to receive(:call)
      allow(CyclicRedirectRemover).to receive(:new).and_return(service)
      Rake::Task["deleted_urls:remove_cycles"].invoke
    end
  end
end
