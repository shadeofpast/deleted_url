require 'rails_helper'

RSpec.describe ChainShortener do
  context 'with long chain' do
    before { create(:chained_deleted_url, chain_size: 12) }

    it 'shorten chain' do
      deleted_urls = DeletedUrl.all.to_a
      described_class.new.call
      expect(deleted_urls.shift.reload.next_url).to eq deleted_urls.last.next_url
      deleted_urls.each do |deleted_url|
        expect { deleted_url.reload }.not_to change { deleted_url.next_url }
      end
    end
  end

  context 'with short chain' do
    before { create(:chained_deleted_url, chain_size: 3) }

    it 'did not shorten chain' do
      deleted_urls = DeletedUrl.all.to_a
      described_class.new.call
      deleted_urls.each do |deleted_url|
        expect { deleted_url.reload }.not_to change { deleted_url.next_url }
      end
    end
  end
end
