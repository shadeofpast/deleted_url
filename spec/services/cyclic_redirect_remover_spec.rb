require 'rails_helper'

RSpec.describe CyclicRedirectRemover do
  context 'with cyclic chain' do
    before { create(:cyclic_deleted_url, chain_size: 3) }

    it 'remove first created' do
      first_created = DeletedUrl.order(:created_at).first
      described_class.new.call
      expect { first_created.reload }.to raise_error ActiveRecord::RecordNotFound
    end
  end

  context 'without cyclic chain' do
    before { create(:chained_deleted_url, chain_size: 3) }

    it 'did not remove url' do
      expect { described_class.new.call }.not_to change { DeletedUrl.count }
    end
  end
end
