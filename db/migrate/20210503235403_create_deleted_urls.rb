class CreateDeletedUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :deleted_urls do |t|
      t.string :prev_url, null: false
      t.string :next_url, null: false

      t.timestamps
    end
    add_index :deleted_urls, :prev_url, unique: true
  end
end
