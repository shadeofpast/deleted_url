# frozen_string_literal: true

class CyclicRedirectRemover
  def call
    _each_chains_batch do |chains|
      while chains.any?
        cyclic_chains, chains = chains.partition(&:cyclic?)
        cyclic_chains.each(&:break_cycle)
        ActiveRecord::Associations::Preloader.new.preload(chains.map(&:last), :next_deleted_url)
        chains = chains.reject(&:ended?).each(&:step)
      end
    end
  end

  def _each_chains_batch
    return enum_for(:_each_chains_batch) unless block_given?

    _chained_deleted_urls.find_in_batches do |batch|
      yield batch.map(&DeletedUrlChain.method(:new))
    end
  end

  def _chained_deleted_urls
    DeletedUrl.includes(:next_deleted_url).where.not(next_deleted_url: { id: nil })
  end
end
