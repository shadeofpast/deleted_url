class ChainShortener
  def call
    _each_chain(&:shorten)
  end

  def _each_chain
    return enum_for(:_each_chain) unless block_given?

    _root_deleted_urls.find_in_batches do |batch|
      chains = batch.map(&DeletedUrlChain.method(:new))
      while chains.any?
        ActiveRecord::Associations::Preloader.new.preload(chains.map(&:last), :next_deleted_url)
        ended_chains, chains = chains.partition(&:ended?)
        ended_chains.each { |chain| yield chain }
        chains = chains.each(&:step).reject(&:cyclic?)
      end
    end
  end

  def _root_deleted_urls
    DeletedUrl.includes(:prev_deleted_url).where(prev_deleted_url: { id: nil })
  end
end
