class DeletedUrl < ApplicationRecord
  belongs_to :next_deleted_url, class_name: 'DeletedUrl',
                                foreign_key: 'next_url', primary_key: 'prev_url',
                                inverse_of: :prev_deleted_url,
                                optional: true

  belongs_to :prev_deleted_url, class_name: 'DeletedUrl',
                                foreign_key: 'prev_url', primary_key: 'next_url',
                                inverse_of: :next_deleted_url,
                                optional: true

  validates :prev_url, uniqueness: true, presence: true
  validates :next_url, presence: true
end
