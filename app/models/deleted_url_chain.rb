  class DeletedUrlChain
    SHORTEN_SIZE = 10 + 2

    attr_reader :deleted_urls
    delegate :any?, :last, to: :deleted_urls
    delegate :next_url, :next_deleted_url, to: :last

    def initialize(deleted_url)
      @deleted_urls = [deleted_url]
    end

    def step
      deleted_urls << last.next_deleted_url
    end

    def cyclic?
      any? { |deleted_url| deleted_url.prev_url == next_url }
    end

    def ended?
      next_deleted_url.nil?
    end

    def break_cycle
      deleted_urls.drop_while { |du| du.prev_url != next_url }.min_by(&:created_at).destroy!
    end

    def shorten
      return if deleted_urls.size < SHORTEN_SIZE

      deleted_urls[0..-SHORTEN_SIZE].each do |deleted_url|
        deleted_url.update!(next_url: next_url)
      end
    end
  end
